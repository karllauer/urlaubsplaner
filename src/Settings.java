import java.io.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.JPasswordField;

public class Settings extends JFrame{

	private JPanel contentPane;	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
//	private JPanel settingsPane;
	
	private static String FolderPath = System.getenv().get("APPDATA") + "\\Urlaubsplaner";
	private static String FilePath = FolderPath + "\\settings";
	private JTextField textIPAdresse;
	private JTextField textUserName;
	private JTextField textPasswort;

	public Settings() {
			setMinimumSize(new Dimension(450, 300));
			setTitle("Server Einstellungen");
	
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 496, 301);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(0, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(new BorderLayout(0, 0));
	
			String dbInfo[] = new String[3];
			dbInfo = readsettings();
			getContentPane().setLayout(null);
			
			JLabel lblIPAdresse = new JLabel("IP Adresse");
			lblIPAdresse.setBounds(23, 33, 99, 25);
			getContentPane().add(lblIPAdresse);
			
			textIPAdresse = new JTextField();
			textIPAdresse.setBounds(23, 69, 210, 20);
			getContentPane().add(textIPAdresse);
			textIPAdresse.setColumns(10);
			textIPAdresse.setText(dbInfo[0]);
			
			JLabel lblUserName = new JLabel("Benutzername");
			lblUserName.setBounds(23, 100, 99, 25);
			getContentPane().add(lblUserName);
			
			textUserName = new JTextField();
			textUserName.setBounds(23, 136, 210, 20);
			getContentPane().add(textUserName);
			textUserName.setColumns(10);
			textUserName.setText(dbInfo[1]);
			
			JLabel lblPasswort = new JLabel("Passwort");
			lblPasswort.setBounds(23, 167, 99, 25);
			getContentPane().add(lblPasswort);

			textPasswort = new JPasswordField();
			textPasswort.setBounds(23, 203, 210, 20);
			getContentPane().add(textPasswort);
			textPasswort.setColumns(10);
			textPasswort.setText(dbInfo[2]);
			
			JButton btnSpeichern = new JButton("Speichern");
			btnSpeichern.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					writesettings(textIPAdresse.getText(),textUserName.getText(),textPasswort.getText());
					dispose();
				}
			});
			
			btnSpeichern.setBounds(289, 101, 100, 23);
			getContentPane().add(btnSpeichern);
			
			JButton btnAbbrechen = new JButton("Abbrechen");
			btnAbbrechen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnAbbrechen.setBounds(289, 168, 100, 23);
			getContentPane().add(btnAbbrechen);
		}
	public static void existsettings() {
	
	File Folder = new File(FolderPath);
	if (!Folder.exists()) {
	    // Folder doesn't exist. Create it
	    if (Folder.mkdir()) {
	        // Folder created
	        File settingsFile = new File(FilePath);
	        if (!settingsFile.exists()) {
	            // File doesn't exists, create it
	            try {
	                if (settingsFile.createNewFile()) {
	                    // settings File created in %APPDATA%\Urlaubsplaner !
	                    BufferedWriter writer = new BufferedWriter(new FileWriter(FilePath));
	                    writer.write("192.168.34.67/urlaub");
	                    writer.newLine();
	                    writer.write("root");
	                    writer.newLine();                    
	                    writer.write("borbarad");
	                    
	                    writer.close();
	                }
	                else {
	                    // Error
	                }
	            } catch (IOException ex) {
	                // Handle exceptions here
	            }
	        }
	        else {
	            // File exists
	        }
	    }
	    else {
	        // Error
	    }
	}
	else {
	    // Folder exists
        File settingsFile = new File(FilePath);
        if (!settingsFile.exists()) {
            // File doesn't exists, create it
            try {
                if (settingsFile.createNewFile()) {
                    // settings File created in %APPDATA%\Urlaubsplaner !
                    BufferedWriter writer = new BufferedWriter(new FileWriter(FilePath));
                    writer.write("192.168.34.67/urlaub");
                    writer.newLine();
                    writer.write("root");
                    writer.newLine();                    
                    writer.write("borbarad");
                    
                    writer.close();
                }
                else {
                    // Error
                }
            } catch (IOException ex) {
                // Handle exceptions here
            }
        }
        else {
            // File exists
        	
        }

	}
}
	public String[] readsettings() {
		String dbInfo[] = new String[3];
	    BufferedReader reader = null;
	    try {
	    	reader = new BufferedReader(new FileReader(FilePath));
	    	dbInfo[0] = reader.readLine();
	    	dbInfo[1] = reader.readLine();
	    	dbInfo[2] = reader.readLine();
	    	reader.close();	
	    	} catch (IOException ex){
	    		ex.printStackTrace();
	    	}
	    return dbInfo;
	    }

	
	
	public void writesettings(String ipAdresse, String userName, String passwort) {
//	String dbInfo[] = new String[3];
	
	
	BufferedWriter writer = null;
	try {
		writer = new BufferedWriter(new FileWriter(FilePath));
        writer.write(ipAdresse);
        writer.newLine();
        writer.write(userName);
        writer.newLine();                    
        writer.write(passwort);
        
        writer.close();
		} catch (IOException ex){
			ex.printStackTrace();
		}
	return;
	}
}