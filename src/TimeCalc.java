import java.util.Calendar;
import java.util.GregorianCalendar;

public class TimeCalc {

	public static Calendar easterSunday(int year) {
		int i = year % 19;
		int j = year / 100;
		int k = year % 100;

		int l = (19 * i + j - (j / 4) - ((j - ((j + 8) / 25) + 1) / 3) + 15) % 30;
		int m = (32 + 2 * (j % 4) + 2 * (k / 4) - l - (k % 4)) % 7;
		int n = l + m - 7 * ((i + 11 * l + 22 * m) / 451) + 114;

		int month = n / 31;
		int day = (n % 31) + 1;

		return new GregorianCalendar(year, month - 1, day);
	}

	public static String feiertage(Calendar date) {
		String feiertag = "";
		String[] nameDesTags = new String[13];
		Calendar[] feier = new GregorianCalendar[13];
		Calendar ostersonntag = TimeCalc.easterSunday(date.get(Calendar.YEAR));

		// Neujahr
		feier[0] = new GregorianCalendar(date.get(Calendar.YEAR), 0, 1);
		nameDesTags[0] = "Neujahr";
		// Maifeiertag
		feier[1] = new GregorianCalendar(date.get(Calendar.YEAR), 4, 1);
		nameDesTags[1] = "Maifeiertag";
		// Tag der dt. Einheit
		feier[2] = new GregorianCalendar(date.get(Calendar.YEAR), 9, 3);
		nameDesTags[2] = "Tag der dt. Einheit";
		// 1. Weihnachtsfeiertag
		feier[3] = new GregorianCalendar(date.get(Calendar.YEAR), 11, 25);
		nameDesTags[3] = "1. Weihnachtsfeiertag";
		// 2. Weihnachtsfeiertag
		feier[4] = new GregorianCalendar(date.get(Calendar.YEAR), 11, 26);
		nameDesTags[4] = "2. Weihnachtsfeiertag";
		// Karfreitag -2
		ostersonntag.add(Calendar.DAY_OF_MONTH, -2);
		feier[5] = new GregorianCalendar(ostersonntag.get(Calendar.YEAR), ostersonntag.get(Calendar.MONTH),
				ostersonntag.get(Calendar.DAY_OF_MONTH));
		nameDesTags[5] = "Karfreitag";
		// Ostermontag +1
		ostersonntag.add(Calendar.DAY_OF_MONTH, 3);
		feier[6] = new GregorianCalendar(ostersonntag.get(Calendar.YEAR), ostersonntag.get(Calendar.MONTH),
				ostersonntag.get(Calendar.DAY_OF_MONTH));
		nameDesTags[6] = "Ostermontag";
		// Christi Himmelfahrt +39
		ostersonntag.add(Calendar.DAY_OF_MONTH, 38);
		feier[7] = new GregorianCalendar(ostersonntag.get(Calendar.YEAR), ostersonntag.get(Calendar.MONTH),
				ostersonntag.get(Calendar.DAY_OF_MONTH));
		;
		nameDesTags[7] = "Christi Himmelfahrt";
		// Pfingstmontag +50
		ostersonntag.add(Calendar.DAY_OF_MONTH, 11);
		feier[8] = new GregorianCalendar(ostersonntag.get(Calendar.YEAR), ostersonntag.get(Calendar.MONTH),
				ostersonntag.get(Calendar.DAY_OF_MONTH));
		nameDesTags[8] = "Pfingstmontag";
		// Fronleichnam +60
		ostersonntag.add(Calendar.DAY_OF_MONTH, 10);
		feier[9] = new GregorianCalendar(ostersonntag.get(Calendar.YEAR), ostersonntag.get(Calendar.MONTH),
				ostersonntag.get(Calendar.DAY_OF_MONTH));
		nameDesTags[9] = "Fronleichnam";
		// Rosenmontag -48
		ostersonntag.add(Calendar.DAY_OF_MONTH, -108);
		feier[10] = new GregorianCalendar(ostersonntag.get(Calendar.YEAR), ostersonntag.get(Calendar.MONTH),
				ostersonntag.get(Calendar.DAY_OF_MONTH));
		nameDesTags[10] = "Rosenmontag";
		// Faschingsdienstag -47
		ostersonntag.add(Calendar.DAY_OF_MONTH, 1);
		feier[11] = new GregorianCalendar(ostersonntag.get(Calendar.YEAR), ostersonntag.get(Calendar.MONTH),
				ostersonntag.get(Calendar.DAY_OF_MONTH));
		nameDesTags[11] = "Faschingsdienstag";
		// Aschermittwoch -46
		ostersonntag.add(Calendar.DAY_OF_MONTH, 1);
		feier[12] = new GregorianCalendar(ostersonntag.get(Calendar.YEAR), ostersonntag.get(Calendar.MONTH),
				ostersonntag.get(Calendar.DAY_OF_MONTH));
		nameDesTags[12] = "Aschermittwoch";

		for (int i = 0; i <= 12; i++) {
			if (feier[i].equals(date)) {
				feiertag = nameDesTags[i];
			}
		}
		return feiertag;
	}
}
