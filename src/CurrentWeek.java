import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JTable;

public class CurrentWeek {
	/**
	 * Berechnungen und Anzeige der Urlaubswoche
	 */
	int weekOfYear;

	Date start;

	Date end;

	public CurrentWeek(final int weekOfYear, final Locale locale) {
		this.weekOfYear = weekOfYear;

		final GregorianCalendar calendar = new GregorianCalendar(locale);
		final int CURRENT_YEAR = calendar.get(Calendar.YEAR);
		calendar.clear();
		calendar.set(Calendar.YEAR, CURRENT_YEAR);
		calendar.set(Calendar.WEEK_OF_YEAR, this.weekOfYear);

		this.start = calendar.getTime();
		calendar.add(Calendar.DAY_OF_MONTH, 6);
		this.end = calendar.getTime();
	}

	public Date getEnd() {
		return end;
	}

	public Date getStart() {
		return start;
	}

	public int getWeekOfYear() {
		return weekOfYear;
	}

	public static int weekNumber() {
		Calendar cal = new GregorianCalendar();
		Date today = Calendar.getInstance().getTime();
		cal.setTime(today);
		int weekOfYear = cal.get(Calendar.WEEK_OF_YEAR);
		return weekOfYear;
	}

	public static JTable overviewJframe(int week) {

		Calendar cal = new GregorianCalendar();

		CurrentWeek currentWeek = new CurrentWeek(week, Locale.GERMANY);
		cal.setTime(currentWeek.getStart());
		String[] heute = new String[5];

		Vector<String> columnNames = new Vector<String>(5);
		DateFormat df = DateFormat.getDateInstance(DateFormat.FULL);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		JTable table = null;

		try {

			Statement stmt = MysqlCon.mysqlConn();

			for (int i = 0; i < 5; i++) {
				heute[i] = format.format(cal.getTime());
				// names of columns
				if (TimeCalc.feiertage(cal).isEmpty()) {
					columnNames.add(df.format(cal.getTime()));
				} else {
					columnNames.add(TimeCalc.feiertage(cal));
				}

				cal.add(Calendar.DAY_OF_MONTH, 1);
			}

			LocalDate localDateStart = currentWeek.getStart().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate localDateStop = currentWeek.getEnd().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			ResultSet rs0 = stmt.executeQuery(
					"select g.utag, CONCAT_WS(', ', m.nname, m.vname) AS name from (urlaub_mitarbeiter AS m, urlaub_genehmigt AS g) where g.utag BETWEEN '"
							+ java.sql.Date.valueOf(localDateStart) + "' AND '" + java.sql.Date.valueOf(localDateStop)
							+ "' AND g.manr=m.manr ORDER BY name, g.utag");

			table = new JTable(buildTableModel(rs0, heute), columnNames);

		} catch (Exception e) {
			System.out.println(e);
		}

		return table;
	}

	public static Vector<Vector<String>> buildTableModel(ResultSet rs0, String[] heute) throws SQLException {

		// data of the table

		Vector<Vector<String>> data = new Vector<Vector<String>>();

		int i = 0;
		Vector<String> vector = new Vector<String>();

		while (rs0.next()) {
			if (i >= 5) {
				data.add(vector);
				vector = new Vector<String>();
				i = 0;
			}

			if (rs0.getString(1).equals(heute[i])) {
				vector.add(rs0.getString(2));
				i++;
			} else {

				vector.addElement("");
				i++;
				rs0.previous();
			}

		}
		data.add(vector);

		return data;

	}
}
