import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.eclipse.wb.swing.FocusTraversalOnArray;

public class Benutzerverwaltung {
	private static JTextField tfMitarbeiternummer;
	private static JTextField tfNachname;
	private static JTextField tfVorname;
	private static JTextField tfGeburtstag;
	private static JTextField tfMitarbeiterkrzel;
	private static JTextField tfZimmernummer;
	private static JTextField tfEintrittsdatum;
	private static JTextField tfResturlaub;
	private static JTextField tfAnzahlUrlaubstage;
	private static JTextField tfWindowsBenutzername;

	private static int manr;
	private static String nname;
	private static String vname;
	private static String geb; // Geburtstag vorerst nur als String ausgelesen
	private static String makz;
	private static int zinr;
	private static int eintr;
	private static double reu;
	private static double anzurl;
	private static int urlvar;
	private static int einmon;
	private static int Inaktiv;
	private static String username;
	
	// TODO Speichern der Daten
	// TODO Anfangsbestände der Urlaubsjahre editierbar machen
	

	public static void main(String[] argv) throws Exception {

		// JComponent com = new DraggableComponent();
		JDialog f = new JDialog();
		f.setMinimumSize(new Dimension(550, 550));
		f.setModalityType(ModalityType.APPLICATION_MODAL);
		// JFrame f = new JFrame();
		f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		f.getContentPane().setLayout(new BorderLayout(0, 0));

		JMenuBar menuBar = new JMenuBar();
		f.getContentPane().add(menuBar, BorderLayout.NORTH);

		JMenu mnBenutzer = new JMenu("Benutzer");
		menuBar.add(mnBenutzer);

		JMenuItem mntmSpeichern = new JMenuItem("Speichern");
		mnBenutzer.add(mntmSpeichern);

		JMenuItem mntmAbbrechen = new JMenuItem("Abbrechen");
		mnBenutzer.add(mntmAbbrechen);

		JMenuItem mntmNeuenBenutzerffnen = new JMenuItem("neuen Benutzer \u00F6ffnen");
		/*
		 * mntmNeuenBenutzerffnen.addActionListener(new ActionListener() {
		 * public void actionPerformed(ActionEvent arg0) { MysqlCon i = new
		 * MysqlCon(); Benutzerverwaltung.pulldata(i.mitarbeiterAuswahlAlle());
		 * // if (Benutzerverwaltung.urlvar ==1 ) {
		 * chckbxUrlaubsberechnungVariabel.setSelected(Benutzerverwaltung.urlvar
		 * == 1); // } // if (Benutzerverwaltung.Inaktiv == 1) {
		 * Benutzerverwaltung.chckbxInaktiv.setSelected(Benutzerverwaltung.
		 * Inaktiv == 1); // } } });
		 */
		mnBenutzer.add(mntmNeuenBenutzerffnen);

		JPanel panel = new JPanel();
		f.getContentPane().add(panel, BorderLayout.CENTER);

		JLabel lblMitarbeiternummer = new JLabel("Mitarbeiternummer");
		lblMitarbeiternummer.setVerticalAlignment(SwingConstants.TOP);

		JLabel lblNachname = new JLabel("Nachname");
		lblNachname.setVerticalAlignment(SwingConstants.TOP);

		JLabel lblVorname = new JLabel("Vorname");
		lblVorname.setVerticalAlignment(SwingConstants.TOP);

		JLabel lblGeburtstag = new JLabel("Geburtstag");
		lblGeburtstag.setVerticalAlignment(SwingConstants.TOP);

		tfMitarbeiternummer = new JTextField();
		tfMitarbeiternummer.setColumns(10);

		tfNachname = new JTextField();
		tfNachname.setColumns(10);

		tfVorname = new JTextField();
		tfVorname.setColumns(10);

		tfGeburtstag = new JTextField();
		tfGeburtstag.setColumns(10);

		JLabel lblMitarbeiterkrzel = new JLabel("Mitarbeiterk\u00FCrzel");
		lblMitarbeiterkrzel.setVerticalAlignment(SwingConstants.TOP);

		tfMitarbeiterkrzel = new JTextField();
		tfMitarbeiterkrzel.setColumns(10);

		JLabel lblZimmernummer = new JLabel("Zimmernummer");
		lblZimmernummer.setVerticalAlignment(SwingConstants.TOP);

		tfZimmernummer = new JTextField();
		tfZimmernummer.setColumns(10);

		JLabel lblEintrittsdatum = new JLabel("Eintrittsdatum");
		lblEintrittsdatum.setVerticalAlignment(SwingConstants.TOP);

		tfEintrittsdatum = new JTextField();
		tfEintrittsdatum.setColumns(10);

		JLabel lblResturlaub = new JLabel("Resturlaub 31.12.06");
		lblResturlaub.setVerticalAlignment(SwingConstants.TOP);

		tfResturlaub = new JTextField();
		tfResturlaub.setColumns(10);

		JLabel lblAnzahlUrlaubstage = new JLabel("Anzahl Urlaubstage");
		lblAnzahlUrlaubstage.setVerticalAlignment(SwingConstants.TOP);

		tfAnzahlUrlaubstage = new JTextField();
		tfAnzahlUrlaubstage.setColumns(10);

		JLabel lblUrlaubsberechnungVariabel = new JLabel("Urlaubsberechnung variabel");
		lblUrlaubsberechnungVariabel.setVerticalAlignment(SwingConstants.TOP);

		JLabel lblEintrittsmonat = new JLabel("Eintrittsmonat");

		JComboBox<Integer> comboBoxEintrittsmonat = new JComboBox<>();
		comboBoxEintrittsmonat
				.setModel(new DefaultComboBoxModel<Integer>(new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }));
		comboBoxEintrittsmonat.setMaximumRowCount(12);

		comboBoxEintrittsmonat.setSelectedItem(Benutzerverwaltung.einmon);

		JLabel lblInaktiv = new JLabel("Inaktiv");

		JLabel lblWindowsBenutzername = new JLabel("Windows Benutzername");

		tfWindowsBenutzername = new JTextField();
		tfWindowsBenutzername.setColumns(10);

		JCheckBox chckbxUrlaubsberechnungVariabel = new JCheckBox("");

		JCheckBox chckbxInaktiv = new JCheckBox("");

		mntmNeuenBenutzerffnen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MysqlCon i = new MysqlCon();
				Benutzerverwaltung.pulldata(i.mitarbeiterAuswahlAlle());
				chckbxUrlaubsberechnungVariabel.setSelected(Benutzerverwaltung.urlvar == 1);
				chckbxInaktiv.setSelected(Benutzerverwaltung.Inaktiv == 1);
				comboBoxEintrittsmonat.setSelectedItem(Benutzerverwaltung.einmon);
			}
		});

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup().addContainerGap().addGroup(gl_panel
								.createParallelGroup(Alignment.TRAILING).addGroup(
										gl_panel.createSequentialGroup().addComponent(lblWindowsBenutzername)
												.addPreferredGap(ComponentPlacement.RELATED, 238, Short.MAX_VALUE)
												.addComponent(tfWindowsBenutzername, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel.createSequentialGroup().addComponent(lblInaktiv)
										.addPreferredGap(ComponentPlacement.RELATED, 309, Short.MAX_VALUE)
										.addComponent(chckbxInaktiv))
								.addGroup(gl_panel.createSequentialGroup().addComponent(lblUrlaubsberechnungVariabel)
										.addPreferredGap(ComponentPlacement.RELATED, 284, Short.MAX_VALUE).addComponent(
												chckbxUrlaubsberechnungVariabel))
								.addGroup(
										gl_panel.createSequentialGroup().addComponent(lblResturlaub)
												.addPreferredGap(ComponentPlacement.RELATED, 254, Short.MAX_VALUE)
												.addComponent(tfResturlaub, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel.createSequentialGroup().addComponent(lblEintrittsdatum)
										.addPreferredGap(ComponentPlacement.RELATED, 286, Short.MAX_VALUE).addComponent(
												tfEintrittsdatum,
												GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel.createSequentialGroup().addGroup(gl_panel
										.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
												.addComponent(lblMitarbeiternummer, Alignment.LEADING,
														GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addComponent(lblNachname, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
														90, Short.MAX_VALUE)
												.addComponent(lblVorname, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addComponent(lblGeburtstag, GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addComponent(tfNachname, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(tfVorname, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(tfGeburtstag, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(tfMitarbeiternummer, GroupLayout.PREFERRED_SIZE, 86,
														GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_panel.createSequentialGroup()
										.addComponent(lblMitarbeiterkrzel, GroupLayout.DEFAULT_SIZE, 349,
												Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(tfMitarbeiterkrzel,
												GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel.createSequentialGroup()
										.addComponent(lblZimmernummer, GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(tfZimmernummer,
												GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addComponent(lblAnzahlUrlaubstage).addComponent(lblEintrittsmonat))
										.addPreferredGap(ComponentPlacement.RELATED, 260, Short.MAX_VALUE)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
												.addComponent(comboBoxEintrittsmonat, 0, GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addComponent(tfAnzahlUrlaubstage))))
								.addGap(85)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblMitarbeiternummer)
								.addComponent(tfMitarbeiternummer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNachname).addComponent(tfNachname, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblVorname).addComponent(tfVorname, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(tfGeburtstag, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblGeburtstag))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblMitarbeiterkrzel)
								.addComponent(tfMitarbeiterkrzel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblZimmernummer)
								.addComponent(tfZimmernummer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblEintrittsdatum)
								.addComponent(tfEintrittsdatum, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblResturlaub)
								.addComponent(tfResturlaub, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblAnzahlUrlaubstage)
								.addComponent(tfAnzahlUrlaubstage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblUrlaubsberechnungVariabel)
								.addComponent(chckbxUrlaubsberechnungVariabel))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblEintrittsmonat)
								.addComponent(comboBoxEintrittsmonat, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblInaktiv)
								.addComponent(chckbxInaktiv))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblWindowsBenutzername)
								.addComponent(tfWindowsBenutzername, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(147, Short.MAX_VALUE)));
		panel.setLayout(gl_panel);
		panel.setFocusTraversalPolicy(new FocusTraversalOnArray(
				new Component[] { lblMitarbeiternummer, tfMitarbeiternummer, lblNachname, tfNachname, lblVorname,
						tfVorname, lblGeburtstag, tfGeburtstag, lblMitarbeiterkrzel, tfMitarbeiterkrzel,
						lblZimmernummer, tfZimmernummer, lblEintrittsdatum, tfEintrittsdatum, lblResturlaub }));
		f.setSize(544, 506);
		f.setVisible(true);

		// f.add(com);
		// f.setSize(300, 300);
		// f.setVisible(true);
	}

	private static void pulldata(String username) {

		Statement stmt = MysqlCon.mysqlConn();
		ResultSet rs1;
		try {
			rs1 = stmt.executeQuery("select * from urlaub_mitarbeiter where username='" + username + "'");
			rs1.next();
			Benutzerverwaltung.manr = rs1.getInt("manr");
			Benutzerverwaltung.nname = rs1.getString("nname");
			Benutzerverwaltung.vname = rs1.getString("vname");
			Benutzerverwaltung.geb = rs1.getString("geb"); // Geburtstag vorerst
															// nur als String
															// ausgelesen
			Benutzerverwaltung.makz = rs1.getString("makz");
			Benutzerverwaltung.zinr = rs1.getInt("zinr");
			Benutzerverwaltung.eintr = rs1.getInt("eintr");
			Benutzerverwaltung.reu = rs1.getDouble("reu");
			Benutzerverwaltung.anzurl = rs1.getDouble("anzurl");
			Benutzerverwaltung.urlvar = rs1.getInt("urlvar");
			Benutzerverwaltung.einmon = rs1.getInt("einmon");
			Benutzerverwaltung.Inaktiv = rs1.getInt("Inaktiv");
			Benutzerverwaltung.username = rs1.getString("username");

		} catch (SQLException ex) {
			System.out.println("message"+ ex);
			ex.printStackTrace();
		}

		Benutzerverwaltung.tfMitarbeiternummer.setText(String.valueOf(Benutzerverwaltung.manr));
		Benutzerverwaltung.tfNachname.setText(Benutzerverwaltung.nname);
		Benutzerverwaltung.tfVorname.setText(Benutzerverwaltung.vname);
		Benutzerverwaltung.tfGeburtstag.setText(Benutzerverwaltung.geb);
		Benutzerverwaltung.tfMitarbeiterkrzel.setText(Benutzerverwaltung.makz);
		Benutzerverwaltung.tfZimmernummer.setText(String.valueOf(Benutzerverwaltung.zinr));
		Benutzerverwaltung.tfEintrittsdatum.setText(String.valueOf(Benutzerverwaltung.eintr));
		Benutzerverwaltung.tfResturlaub.setText(String.valueOf(Benutzerverwaltung.reu));
		Benutzerverwaltung.tfAnzahlUrlaubstage.setText(String.valueOf(Benutzerverwaltung.anzurl));
		;
		Benutzerverwaltung.tfWindowsBenutzername.setText(Benutzerverwaltung.username);
		;
	}
}

class DraggableComponent extends JComponent implements DragGestureListener, DragSourceListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	DragSource dragSource;

	public DraggableComponent() {
		dragSource = new DragSource();
		dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY_OR_MOVE, this);
	}

	@Override
	public void dragGestureRecognized(DragGestureEvent evt) {
		Transferable t = new StringSelection("aString");
		dragSource.startDrag(evt, DragSource.DefaultCopyDrop, t, this);
	}

	@Override
	public void dragEnter(DragSourceDragEvent evt) {
		System.out.println("enters");
	}

	@Override
	public void dragOver(DragSourceDragEvent evt) {

		System.out.println("over");
	}

	@Override
	public void dragExit(DragSourceEvent evt) {
		System.out.println("leaves");
	}

	@Override
	public void dropActionChanged(DragSourceDragEvent evt) {
		System.out.println("changes the drag action between copy or move");
	}

	@Override
	public void dragDropEnd(DragSourceDropEvent evt) {
		System.out.println("finishes or cancels the drag operation");
	}
}
