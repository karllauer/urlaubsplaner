import java.sql.*;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Calendar;

//import java.time.LocalDate;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

class MysqlCon {

	public static Statement mysqlConn() {
		Settings settings = new Settings();
		String dbInfo[] = new String[3];
		dbInfo = settings.readsettings();
		Statement stmt = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// Connection con =
			// DriverManager.getConnection("jdbc:mysql://192.168.34.39:3306/urlaub",
			// "root", "borbarad");
			Connection con = DriverManager.getConnection("jdbc:mysql://" + dbInfo[0] + "", dbInfo[1], dbInfo[2]);

			// here urlaub is database name, root is username
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println(e);
		}
		return stmt;
	}

	public JTable connection() {

		JTable table = new JTable();
		String mitarbeiter = mitarbeiterAuswahlAktiv();
		int year = jahrAbfrage();
		int year2 = year + 1;
		int manr;
		try {
			Statement stmt = MysqlCon.mysqlConn();
			if (mitarbeiter.isEmpty()) {
				mitarbeiter = System.getProperty("user.name");
			}

			ResultSet rs1 = stmt
					.executeQuery("select manr from urlaub_mitarbeiter where username='" + mitarbeiter + "'");
			rs1.next();
			manr = rs1.getInt("manr");

			String timezone = "set lc_time_names = 'de_DE'";
			stmt.executeQuery(timezone);
			String abfrage = "select DATE_FORMAT(utag,'%W %d.%m.%x'), uart, gvon, tag from urlaub_genehmigt where manr='"
					+ manr + "' AND utag BETWEEN '" + year + "-01-01' AND '" + year2 + "-01-01'";
			ResultSet rs = stmt.executeQuery(abfrage);

			// It creates and displays the table
			table = new JTable(buildTableModel(rs));
			table.getColumn("tag").setMinWidth(0);
			table.getColumn("tag").setMaxWidth(0);

			double amount = 0;
			double total = 0;
			for (int i = 0; i < table.getRowCount(); i++) {

				amount = 0;
				amount = Double.parseDouble(table.getValueAt(i, 3).toString());
				total += amount;

			}
			System.out.println(total);

		} catch (Exception e) {
			System.out.println(e);
		}
		return table;
	}

	public JTable connection(String mitarbeiter, int year) {

		int year2 = year + 1;
		JTable table = new JTable();
		int manr;

		try {
			Statement stmt = MysqlCon.mysqlConn();
			if (mitarbeiter.isEmpty()) {
				mitarbeiter = System.getProperty("user.name");
			}

			System.out.println(mitarbeiter);
			ResultSet rs1 = stmt
					.executeQuery("select manr from urlaub_mitarbeiter where username='" + mitarbeiter + "'");
			if (!rs1.isBeforeFirst()) {
				mitarbeiter = mitarbeiterAuswahlAktiv();
				ResultSet rs2 = stmt
						.executeQuery("select manr from urlaub_mitarbeiter where username='" + mitarbeiter + "'");
				rs2.next();
				manr = rs2.getInt("manr");
			} else {
				rs1.next();
				manr = rs1.getInt("manr");
			}
			System.out.println(manr);
			String timezone = "set lc_time_names = 'de_DE'";
			stmt.executeQuery(timezone);
			String abfrage = "select DATE_FORMAT(utag,'%W %d.%m.%x'), uart, gvon, tag from urlaub_genehmigt where manr='"
					+ manr + "' AND utag BETWEEN '" + year + "-01-01' AND '" + year2 + "-01-01'";
			ResultSet rs = stmt.executeQuery(abfrage);

			// It creates and displays the table
			table = new JTable(buildTableModel(rs));
			table.getColumn("tag").setMinWidth(0);
			table.getColumn("tag").setMaxWidth(0);

		} catch (Exception e) {
			System.out.println(e);
		}
		return table;
	}

	public String getMitarbeiterName(String systemUser) {
		String mitarbeiter = null;

		try {
			Statement stmt = MysqlCon.mysqlConn();
			ResultSet rs1 = stmt.executeQuery("select * from urlaub_mitarbeiter where username='" + systemUser + "'");
			if (!rs1.isBeforeFirst()) {
				systemUser = mitarbeiterAuswahlAktiv();
				ResultSet rs2 = stmt
						.executeQuery("select * from urlaub_mitarbeiter where username='" + systemUser + "'");
				rs2.next();
				mitarbeiter = rs2.getString("nname") + ", " + rs2.getString("vname");
			} else {
				rs1.next();
				mitarbeiter = rs1.getString("nname") + ", " + rs1.getString("vname");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return mitarbeiter;
	}

	public static String mitarbeiterAuswahlAktiv() {

		String[] choices = null;
		String[] nameChoices = null;
		try {
			Statement stmt = MysqlCon.mysqlConn();
			ResultSet rs = stmt.executeQuery(
					"SELECT username, nname, vname FROM urlaub_mitarbeiter WHERE Inaktiv=0 ORDER BY nname");

			ArrayList<String> uname = new ArrayList<String>();
			ArrayList<String> mitarbeiterName = new ArrayList<String>();

			while (rs.next()) {
				uname.add(rs.getString(1));
				mitarbeiterName.add(rs.getString(2) + ", " + rs.getString(3));
			}

			choices = new String[uname.size()];
			choices = uname.toArray(choices);

			nameChoices = new String[mitarbeiterName.size()];
			nameChoices = mitarbeiterName.toArray(nameChoices);

		} catch (Exception e) {
			System.out.println(e);
		}

		String name = (String) JOptionPane.showInputDialog(null/* frame */, "Welcher Mitarbeiter?", "Auswahl",
				JOptionPane.QUESTION_MESSAGE, null, nameChoices, nameChoices[0]);
		if (name == null) {
			Urlaubsplaner.run();

		}
		int arrayZahl = java.util.Arrays.asList(nameChoices).indexOf(name);

		return choices[arrayZahl]; // liefert Username zur�ck

	}

	public String mitarbeiterAuswahlAlle() {

		String[] choices = null;
		String[] nameChoices = null;
		try {
			Statement stmt = MysqlCon.mysqlConn();
			ResultSet rs = stmt.executeQuery("SELECT username, nname, vname FROM urlaub_mitarbeiter ORDER BY nname");

			ArrayList<String> uname = new ArrayList<String>();
			ArrayList<String> mitarbeiterName = new ArrayList<String>();

			while (rs.next()) {
				uname.add(rs.getString(1));
				mitarbeiterName.add(rs.getString(2) + ", " + rs.getString(3));
			}

			choices = new String[uname.size()];
			choices = uname.toArray(choices);

			nameChoices = new String[mitarbeiterName.size()];
			nameChoices = mitarbeiterName.toArray(nameChoices);

		} catch (Exception e) {
			System.out.println(e);
		}

		String name = (String) JOptionPane.showInputDialog(null/* frame */, "Welcher Mitarbeiter?", "Auswahl",
				JOptionPane.QUESTION_MESSAGE, null, nameChoices, nameChoices[0]);
		if (name == null) {
			Urlaubsplaner.run();

		}
		int arrayZahl = java.util.Arrays.asList(nameChoices).indexOf(name);

		return choices[arrayZahl]; // liefert Username zur�ck

	}

	public int jahrAbfrage() { // Auswahldialog des anzuzeigenden Jahres. Min.
								// 2007 / Max Akt. Jahr +1
		int counter = Calendar.getInstance().get(Calendar.YEAR) - 2007;
		String choices[] = new String[counter + 2];
		for (int i = 0; i <= counter + 1; i++) {

			choices[i] = String.valueOf(i + 2007);
		}

		String eingabe = (String) JOptionPane.showInputDialog(null, "Welches Jahr?", "Auswahl",
				JOptionPane.QUESTION_MESSAGE, null, choices, choices[counter]);
		if (eingabe == null) {
			Urlaubsplaner.run();

		}
		int jahr = Integer.parseInt(eingabe);
		return jahr;
	}

	public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(renameSqlData(metaData.getColumnName(column)));
		}

		Vector<Vector<String>> data = new Vector<Vector<String>>();
		while (rs.next()) {
			Vector<String> vector = new Vector<String>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				vector.add(renameSqlData(rs.getString(columnIndex)));
			}
			data.add(vector);
		}

		return new DefaultTableModel(data, columnNames);

	}

	public static String renameSqlData(String str) {
		String fqn;

		switch (str) {
		case "manr":
			fqn = "Mitarbeiternummer";
			break;
		case "DATE_FORMAT(utag,'%W %d.%m.%x')":
			fqn = "Urlaubstag";
			break;
		case "utag":
			fqn = "Urlaubstag";
			break;
		case "uart":
			fqn = "Urlaubsart";
			break;
		case "gvon":
			fqn = "genehmigt von";
			break;
		case "VU":
			fqn = "voller Urlaubstag";
			break;
		case "HU":
			fqn = "halber Urlaubstag";
			break;
		case "sc":
			fqn = "C. Sattler";
			break;
		default:
			fqn = str;
			break;
		}
		return fqn;
	}

}
