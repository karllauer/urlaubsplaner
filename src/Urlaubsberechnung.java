import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Urlaubsberechnung {

	public static int abfrageJahresurlaub(String userName, int jahr) {
		int jahresurlaub = 0;
		int manr;

		try {
			Statement stmt = MysqlCon.mysqlConn();
			if (userName.isEmpty()) {
				userName = System.getProperty("user.name");
			}
			ResultSet rs1 = stmt
					.executeQuery("select manr from urlaub_mitarbeiter where username='" + userName + "'");
			if (!rs1.isBeforeFirst()) {
				userName = MysqlCon.mitarbeiterAuswahlAktiv();
				ResultSet rs2 = stmt
						.executeQuery("select manr from urlaub_mitarbeiter where username='" + userName + "'");
				rs2.next();
				manr = rs2.getInt("manr");
			} else {
				rs1.next();
				manr = rs1.getInt("manr");
			}
		
		
		ResultSet rs3;

			rs3 = stmt
					.executeQuery("select * from urlaub_jahresurlaub where manr='" + manr + "' and jahr=" + jahr + "");
			rs3.next();
			
			jahresurlaub = rs3.getInt("gesamturlaub");
		} catch (SQLException ex) {
			System.out.println("message"+ ex);
            ex.printStackTrace();
		}


		return jahresurlaub;
	}
}
