import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.util.Arrays;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class Arbeitsflaeche extends JFrame {

	/**
	 * TODO Berechnung der Urlaubstage
	 * TODO Beantragung von Urlaubstagen
	 * TODO Löschung von eigenen Urlaubsanträgen und Tagen
	 */
	private static final long serialVersionUID = 2L;
	private JPanel contentPane;
	private int showWeek = CurrentWeek.weekNumber();
	private int showYear = Calendar.getInstance().get(Calendar.YEAR);
	private JTable workingTable = null;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Arbeitsflaeche frame = new Arbeitsflaeche();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Arbeitsflaeche() {

		setMinimumSize(new Dimension(640, 600));
		setTitle("Urlaubs\u00FCbersicht");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		JLabel lblInfoLabel = new JLabel("KW " + Integer.toString(showWeek));
		panel.add(lblInfoLabel, BorderLayout.NORTH);

		workingTable = CurrentWeek.overviewJframe(showWeek);
		
		JLabel lblJahresurlaub = new JLabel("Jahresurlaub");
		lblJahresurlaub.setVisible(false);
		lblJahresurlaub.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(lblJahresurlaub, BorderLayout.SOUTH);
		JScrollPane scrollPane = new JScrollPane(workingTable);
		panel.add(scrollPane, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new BorderLayout(0, 0));

		JButton btnPreviousButton = new JButton("Vorherige Woche");
		btnPreviousButton.setMinimumSize(new Dimension(161, 23));
		btnPreviousButton.setMaximumSize(new Dimension(161, 23));
		btnPreviousButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showWeek--;
				panel.removeAll();
				JLabel lblKWLabel = new JLabel("KW " + Integer.toString(calendarWeek(showWeek)));
				panel.add(lblKWLabel, BorderLayout.NORTH);
				
				workingTable = CurrentWeek.overviewJframe(showWeek);
				JScrollPane scrollPane = new JScrollPane(workingTable);
				panel.add(scrollPane, BorderLayout.CENTER);
				panel.updateUI();
			}
		});

		JButton btnPreviousYearButton = new JButton("Vorheriges Jahr");
		btnPreviousYearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				showYear--;
				panel.removeAll();
				MysqlCon connect = new MysqlCon();
				JLabel lblInfoLabel = new JLabel(
						connect.getMitarbeiterName(System.getProperty("user.name")) + " " + Integer.toString(showYear));
				panel.add(lblInfoLabel, BorderLayout.NORTH);
				
				workingTable = connect.connection(System.getProperty("user.name"), showYear);
				JScrollPane scrollPane = new JScrollPane(workingTable);
				panel.add(scrollPane, BorderLayout.CENTER);

				panel.add(lblJahresurlaub, BorderLayout.SOUTH);
				lblJahresurlaub.setVisible(true);
				panel.updateUI();

			}
		});
		panel_1.add(btnPreviousButton, BorderLayout.WEST);

		JButton btnNextButton = new JButton("N\u00E4chste Woche");
		btnNextButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				showWeek++;
				panel.removeAll();
				JLabel lblKWLabel = new JLabel("KW " + Integer.toString(calendarWeek(showWeek)));
				panel.add(lblKWLabel, BorderLayout.NORTH);
				
				workingTable = CurrentWeek.overviewJframe(showWeek);
				JScrollPane scrollPane = new JScrollPane(workingTable);
				panel.add(scrollPane, BorderLayout.CENTER);
				panel.updateUI();
			}
		});

		JButton btnNextYearButton = new JButton("N\u00E4chstes Jahr");
		btnNextYearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showYear++;
				panel.removeAll();
				MysqlCon connect = new MysqlCon();
				JLabel lblInfoLabel = new JLabel(
						connect.getMitarbeiterName(System.getProperty("user.name")) + " " + Integer.toString(showYear));
				panel.add(lblInfoLabel, BorderLayout.NORTH);
				workingTable = connect.connection(System.getProperty("user.name"), showYear);
				JScrollPane scrollPane = new JScrollPane(workingTable);
				panel.add(scrollPane, BorderLayout.CENTER);
				panel.add(lblJahresurlaub, BorderLayout.SOUTH);
				lblJahresurlaub.setVisible(true);
				panel.updateUI();

			}
		});
		panel_1.add(btnNextButton, BorderLayout.EAST);

		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.CENTER);

		JButton btnGoOnButton = new JButton("Abfrage Urlaub Mitarbeiter");
		panel_2.add(btnGoOnButton);

		JMenuBar menuBar = new JMenuBar();
		contentPane.add(menuBar, BorderLayout.NORTH);

		JMenu mnProgramm = new JMenu("Programm");
		menuBar.add(mnProgramm);

		JMenuItem mntmBeenden = new JMenuItem("Beenden");
		mntmBeenden.setIcon(
				new ImageIcon(Arbeitsflaeche.class.getResource("/javax/swing/plaf/metal/icons/ocean/close.gif")));
		mntmBeenden.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		JMenuItem mntmDrucken = new JMenuItem("Drucken");
//		mntmDrucken.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				
//				try {
//
//					JTable printJob = new JTable();
//					printJob = CurrentWeek.overviewJframe(showWeek);
//					printJob.setSize(printJob.getPreferredSize());
//					
//					JFrame printframe = new JFrame();
//				    printframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//				    JPanel printjPanel = new JPanel(new GridLayout(2, 0));
//				    printjPanel.setOpaque(true);
//				    printJob.setPreferredScrollableViewportSize(new Dimension(500, 70));
//				    printjPanel.add(new JScrollPane(printJob));
//				    /* Add the panel to the JFrame */
//				    printframe.add(printjPanel);
//				    /* Display the JFrame window */
//				    printframe.pack();
////				    printframe.setVisible(true);
//
//					
//					boolean complete = printJob.print(JTable.PrintMode.FIT_WIDTH, null, null, true, null, true);
//				    if (complete) {
//				        /* show a success message  */
//				        
//				    } else {
//				        /*show a message indicating that printing was cancelled */
//				        
//				    }
//				    printframe.dispose();
//				} catch (PrinterException pe) {
//				    /* Printing failed, report to the user */
//				    
//				}
//					
//			}   
//		}); 
		mnProgramm.add(mntmDrucken);
		mnProgramm.add(mntmBeenden);

		JMenu mnGeheZu = new JMenu("Gehe zu...");
		menuBar.add(mnGeheZu);

		JMenuItem mntmMeinemUrlaub = new JMenuItem("meinem Urlaub");
		mntmMeinemUrlaub.setIcon(new ImageIcon(
				Arbeitsflaeche.class.getResource("/com/sun/java/swing/plaf/windows/icons/NewFolder.gif")));
		mntmMeinemUrlaub.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.removeAll();
				panel_1.remove(btnNextButton);
				panel_1.remove(btnPreviousButton);
				MysqlCon connect = new MysqlCon();
				JLabel lblInfoLabel = new JLabel(
						connect.getMitarbeiterName(System.getProperty("user.name")) + " " + Integer.toString(showYear));
				panel.add(lblInfoLabel, BorderLayout.NORTH);
				workingTable = connect.connection(System.getProperty("user.name"), showYear);
				JScrollPane scrollPane = new JScrollPane(workingTable);
				panel.add(scrollPane, BorderLayout.CENTER);
				panel.add(lblJahresurlaub, BorderLayout.SOUTH);
				panel_1.add(btnPreviousYearButton, BorderLayout.WEST);
				panel_1.add(btnNextYearButton, BorderLayout.EAST);
				
				
				lblJahresurlaub.setText("Jahresurlaub " + Integer.toString(Urlaubsberechnung.abfrageJahresurlaub(System.getProperty("user.name"), showYear)));
				lblJahresurlaub.setVisible(true);
				panel.updateUI();
				panel_1.updateUI();
			}
		});
		mnGeheZu.add(mntmMeinemUrlaub);

		JMenuItem mntmUrlaubAktuelleWoche = new JMenuItem("Urlaub aktuelle Woche");
		mntmUrlaubAktuelleWoche.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showWeek = CurrentWeek.weekNumber();
				panel.removeAll();
				panel_1.removeAll();
				JLabel lblKWLabel = new JLabel("KW " + Integer.toString(showWeek));
				panel.add(lblKWLabel, BorderLayout.NORTH);
				workingTable = CurrentWeek.overviewJframe(showWeek);
				JScrollPane scrollPane = new JScrollPane(workingTable);
				panel.add(scrollPane, BorderLayout.CENTER);
				panel_1.add(btnNextButton, BorderLayout.EAST);
				panel_1.add(btnPreviousButton, BorderLayout.WEST);
				panel_1.add(panel_2, BorderLayout.CENTER);
				panel_2.add(btnGoOnButton, BorderLayout.NORTH);
				panel.updateUI();
				panel_1.updateUI();
				panel_2.updateUI();

			}
		});
		mnGeheZu.add(mntmUrlaubAktuelleWoche);

		JMenu mnEinstellungen = new JMenu("Einstellungen");
		menuBar.add(mnEinstellungen);

		System.out.println(System.getProperty("user.name"));

		String[] admins = new String[] { "Dennis", "D.Ott", "I.Eisenbeis" };
		String user = System.getProperty("user.name");
		if (Arrays.asList(admins).contains(user)) {
			JMenuItem mntmServer = new JMenuItem("Server");
			mntmServer.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					Settings frame = new Settings();
					frame.setVisible(true);

				}
			});
			mnEinstellungen.add(mntmServer);

			JMenuItem mntmBenutzerverwaltung = new JMenuItem("Benutzerverwaltung");
			mntmBenutzerverwaltung.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					contentPane.setEnabled(false);
					try {
						Benutzerverwaltung.main(null);
					} catch (Exception ex) {
						System.out.println("message"+ ex);
						ex.printStackTrace();
					}
					contentPane.setEnabled(true);
				}
			});
			mnEinstellungen.add(mntmBenutzerverwaltung);
		}

		btnGoOnButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.removeAll();
				MysqlCon connect = new MysqlCon();
				workingTable = connect.connection();
				JScrollPane scrollPane = new JScrollPane(workingTable);
				panel.add(scrollPane, BorderLayout.NORTH);
				panel.add(lblJahresurlaub, BorderLayout.SOUTH);
				lblJahresurlaub.setVisible(true);
				panel.updateUI();
			}
		});
		mntmDrucken.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			
			try {

				boolean complete = workingTable.print(JTable.PrintMode.FIT_WIDTH, null, null, true, null, true);
			    if (complete) {
			        /* show a success message  */
			        
			    } else {
			        /*show a message indicating that printing was cancelled */
			        
			    }

			} catch (PrinterException pe) {
			    /* Printing failed, report to the user */
			    
			}
				
		}   
	}); 

	}

	private int calendarWeek(int showWeek) {
		int result = showWeek;

		if (showWeek < 1) {
			result = 52 + showWeek;
		}
		if (showWeek > 52) {
			result = showWeek - 52;
		}

		return result;
	}
}
